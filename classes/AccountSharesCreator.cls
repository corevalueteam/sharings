/**
 *  Creates Share records of Account sObject.
 *  The presence of CaseAccessLevel, ContactAccessLevel and OpportunityAccessLevel fields
 *  on AccountShare makes reasonable the separate shares creator for AccountShare's.
 *
 *  @author Bogdan Protsenko
 *  @author Maksym Gorinshteyn
 */
public class AccountSharesCreator extends AbstractSharesCreator{
    //AccountShare specific fields.
    public String caseAccsessLevel = 'Read';
    public String contactAccsessLevel = 'Read';
    public String opportunityAccsessLevel = 'Read';

    public AccountSharesCreator(){
        super(AccountShare.getSobjectType());
    }

    /**
     *  Creates single AccountShare record.
     *  @param parentId record Id to be shared.
     *  @param userOgroupId user or group Id  to be shared with.
     *  @return SObject AccountShare record.
     */
    public SObject createShare(String parentId, String userOrGroupId){
        AccountShare accountShare = new  AccountShare();
        accountShare.AccountId = parentId;
        accountShare.UserOrGroupId = userOrGroupId;
        accountShare.AccountAccessLevel = accessLevel;
        accountShare.CaseAccessLevel = caseAccsessLevel;
        accountShare.ContactAccessLevel = contactAccsessLevel;
        accountShare.OpportunityAccessLevel =  opportunityAccsessLevel;
        accountShare.RowCause =  rawCause;
        return accountShare;
    }

}