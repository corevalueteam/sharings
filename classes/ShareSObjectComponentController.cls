/**
 *  Controller for ShareSObjectComponent lightning component.
 *  @author Maksym Gorinshteyn
 */
public class ShareSObjectComponentController {

    /**
     *  Creates List of hierarchy nodes for concrete SObject.
     *  @param sObjectName specifies the concrete type of SObject.
     *  @param parentField specifies the hierarchy field.
     *  @return list of HierarchyNode instances.
     */
    @AuraEnabled
    public static List<HierarchyNode> getHierarchyNodes(String sObjectName, String parentField) {
        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
        Schema.SObjectField field = sObjectType.getDescribe().fields.getMap().get(parentField);
        HierarchyNodeBuilder builder = new HierarchyNodeBuilder(sObjectType, field);
        return builder.build();
    }

    /**
     *  Creates List of all Sobject's labels in org which have Share objects according to OWD settings.
     *  @return list of Share object's labels.
     */
    @AuraEnabled
    public static List<String> getSharedSObjectNames() {
        return SharedSObjectsService.getSharedSObjectLabels();
    }

    /**
     *  Creates list of wpappers contains all records of concrete SObject with existed shares.
     *  @param sObjectName  specifies the concrete type of SObject.
     *  @param userOgroupId specifies user or group Id SObject records are shared with.
     *  @return list of Share object's labels.
     */
    @AuraEnabled
    public static List<SharedSObjectsService.RecordWrapper> getSharedSObjectRecords(String sObjectName, String userOrGroupId, String searchWord) {
        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
        if (sObjectType == null) {
            sObjectType = Schema.getGlobalDescribe().get(sObjectName + '__c');
        }
        return SharedSObjectsService.getSharedSObjectRecords(sObjectType, userOrGroupId, searchWord);
    }

    /**
     *  Chain queueables and creates actions for inserting and/or deleting shares for concrete SObject.
     *  @param sObjectName  specifies the concrete type of SObject.
     *  @param insertIds specifies Ids of SObject records for inserting shares.
     *  @param deleteIds specifies Ids of SObject records for deleting shares.
     *  @param roleId specifies USerRole for sharing.
     */
    @AuraEnabled
    public static void shareSObjects(String sObjectName, List<String> insertIds, List<String> deleteIds, String roleId) {
        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
        if (sObjectType == null) {
            sObjectType = Schema.getGlobalDescribe().get(sObjectName + '__c');
        }
        SharesCreator sharesCreator = SharesFactory.createSharesCreator(sObjectType);
        Map<String, Set<String>> insertParentToUserOrGroupIds = new Map<String, Set<String>>();
        Group userOrGroup = Database.query('SELECT Id From Group WHERE RelatedId =\'' + roleId + '\' AND Type=\'Role\'');
        Set<String> userOrGroupIds = new Set<String>{
                userOrGroup.Id
        };
        for (Integer i = 0, size = insertIds.size(); i < size; ++i) {
            insertParentToUserOrGroupIds.put(insertIds[i], userOrGroupIds);
        }

        InsertSharesQueueable insertShares = new InsertSharesQueueable(sharesCreator, insertParentToUserOrGroupIds);
        System.enqueueJob(insertShares);

        List<SObject> deletedShares = SharedSObjectsService.getSharesToDelete(sObjectType, deleteIds, roleId);
        DeleteSharesQueueable deleteShares = new DeleteSharesQueueable(deletedShares);
        System.enqueueJob(deleteShares);
    }
}