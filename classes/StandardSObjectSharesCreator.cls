/**
 *  Creates Share records of Asset, Campaign, Case, Contact, Lead, Opportunity and User standard SObjects.
 *  These sObjects have parentId and accessLevel fields pattern like %objectName% + AccessLevel and %objectName% + Id.
 *
 *  @author Bogdan Protsenko
 *  @author Maksym Gorinshteyn
 */
public class StandardSObjectSharesCreator extends AbstractSharesCreator {

    /**
     *   @param shareType specifies the concrete type of Share records.
     */ 
    public StandardSObjectSharesCreator(Schema.SObjectType shareType){
        super(shareType);
    }

    /**
     *  Creates single Share record for specific standard SObjects.
     *  @param parentId record Id to be shared.
     *  @param userOgroupId user or group Id  to be shared with.
     *  @return SObject Share record.
     */
    public SObject createShare(String parentId, String userOrGroupId){
        SObject share = shareType.newSObject();
        String sobjectName =String.valueOf(share.getSObjectType()).remove('Share');
        share.put(sobjectName+'Id', parentId);
        share.put(sobjectName+'AccessLevel', accessLevel);
        share.put('UserOrGroupId', userOrGroupId);
        share.put('RowCause', rawCause);
        return share;
    }
}