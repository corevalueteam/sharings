@isTest
private class AbstractSharesCreatorTest {
	
	@isTest 
	static void testCreateShares() {
		List<Account> accounts = new List<Account>();
		Integer sharesNumber = 100;
		Map<String, Set<String>> parentIdToUserOrGroupIds = new Map<String, Set<String>>();
		Schema.SObjectType shareType = AccountShare.getSobjectType();
		if(shareType!=null){
			Set<String> userOrGroupIds = new Set<String>{UserInfo.getUserId()};
			AbstractSharesCreator testCreator = new AccountSharesCreator();
			for(Integer i=0; i<sharesNumber;++i){
				accounts.add(new Account(Name ='TestAccount '+i));
			}
			insert accounts;
			for(Account account:accounts){
				parentIdToUserOrGroupIds.put(account.Id, userOrGroupIds);
			}
			Test.startTest();
			List<Sobject> shares = testCreator.createShares(parentIdToUserOrGroupIds);
			Test.stopTest();
			String userOrGroupId = SObjectType.AccountShare.fields.UserOrGroupId.Name;
			String accountId = SObjectType.AccountShare.fields.AccountId.Name;
			System.assertEquals(sharesNumber, shares.size());
			for(Integer i=0, size = shares.size(); i < size; ++i){
				System.assertEquals(UserInfo.getUserId(), shares[i].get(userOrGroupId));
				System.assertEquals(accounts[i].Id, shares[i].get(accountId));
			}
		}
	}
}