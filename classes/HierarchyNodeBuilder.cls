/**
 *  Builds hierarchy of HierarchyNode classes for concrete SObject.
 *  @author Maksym Gorinshteyn
 */
public with sharing class HierarchyNodeBuilder {
    private String sObjectName;
    private String parentField;

    /**
     *  Constructor of HierarchyNodeBuilder.
     *  @param sObjType  specifies the concrete type of SObject records.
     *  @param field specifies the hierarchy field.
     */
    public HierarchyNodeBuilder(Schema.SObjectType sObjType, Schema.SobjectField field) {
        this.sObjectName = String.valueOf(sObjType);
        this.parentField = String.valueOf(field);
    }

    /**
     *  Builds hierarchy of concrete SObject.
     *  @return list of HierarchyNode instances.
     */
    public List<HierarchyNode> build() {
        Map<Id, List<SObject>> parentToChildren = getSobjectParentToChildren(sObjectName, parentField);
        List<HierarchyNode> nodes = new List<HierarchyNode>();
        List<SObject> roots = parentToChildren.get(null);
        for (SObject root : roots) {
            String rootId = root.Id;
            HierarchyNode node = new HierarchyNode((String) root.get('Name'), rootId, getChildren(rootId, parentToChildren));

            nodes.add(node);
        }
        return nodes;
    }

    private List<Object> getChildren(String key, Map<Id, List<SObject>> parentToChildren) {
        List<Object> children = new List<Object>();
        for (Sobject child : parentToChildren.get(key)) {
            String childId = child.Id;
            HierarchyNode node = new HierarchyNode((String) child.get('Name'), childId,
                                        parentToChildren.containsKey(childId) ? getChildren(childId, parentToChildren) : new List<Object>());
            children.add(node);
        }
        return children;
    }

    private Map<Id, List<SObject>> getSobjectParentToChildren(String sObjectName, String parentField) {
        Map<Id, List<SObject>> parentToChildren = new Map<Id, List<SObject>>();
        String query = 'SELECT Id, Name, ' + parentField + ' FROM ' + sObjectName;
        List<SObject> sObjects = Database.query(query);
        List<SObject> children;
        String parentId;
        for (Sobject sObj : sObjects) {
            parentId = (Id) sObj.get(parentField);
            if (!parentToChildren.containsKey(parentId)) {
                parentToChildren.put(parentId, new List<SObject>());
            }
            children = parentToChildren.get(parentId);
            children.add(sObj);
        }
        return parentToChildren;
    }
}