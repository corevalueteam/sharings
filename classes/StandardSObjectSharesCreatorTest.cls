@isTest
private class StandardSObjectSharesCreatorTest {
	
	@isTest 
	static void testCreateShare() {
		Lead lead= new Lead(LastName = 'TestLead', Company = 'Company');
		insert lead;
		String leadShareName = SObjectType.LeadShare.Name;
		Schema.SObjectType shareType= Schema.getGlobalDescribe().get(leadShareName);
		if(shareType!=null){
			StandardSObjectSharesCreator standardSharesCreator = new StandardSObjectSharesCreator(shareType);
			String userOrGroupIdFieldName = SObjectType.LeadShare.fields.UserOrGroupId.Name;
			String userOrGroupId =UserInfo.getUserId(); 
			Test.startTest();
			Sobject share = standardSharesCreator.createShare(lead.Id, userOrGroupId);
			Test.stopTest();
			System.assert(share instanceof LeadShare);
			System.assertEquals(UserInfo.getUserId(), share.get(userOrGroupIdFieldName));
		}
	}
	
}