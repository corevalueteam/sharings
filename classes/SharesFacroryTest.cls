@isTest
private class SharesFacroryTest {
	
	@isTest 
	static void testAccountCreateSharesCreator() {
		Schema.SObjectType account=Schema.Account.getSobjectType();
		Test.startTest();																						
		SharesCreator accountSharesCreator =SharesFactory.createSharesCreator(account);
		Test.stopTest();
		System.assert(accountSharesCreator instanceof AccountSharesCreator);
	}
		
	@isTest 
	static void testLeadCreateSharesCreator() {
		Schema.SObjectType lead = Schema.Lead.getSobjectType();
		Test.startTest();																						
		SharesCreator leadSharesCreator =SharesFactory.createSharesCreator(lead);
		Test.stopTest();
		System.assert(leadSharesCreator instanceof StandardSObjectSharesCreator);
		
	}

	@isTest 
	static void testGoalCreateSharesCreator() {
		Schema.SObjectType goal = Schema.Goal.getSobjectType();
		Test.startTest();																						
		SharesCreator goalSharesCreator =SharesFactory.createSharesCreator(goal);
		Test.stopTest();
		System.assert( goalSharesCreator instanceof SObjectSharesCreator);
	}
	
}