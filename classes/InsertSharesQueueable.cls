/**
 *  Queueable that creates and inserts share records.
 *
 *  @author Bogdan Protsenko
 *  @author Maksym Gorinshteyn
 */
public class InsertSharesQueueable implements Queueable {
    private SharesCreator sharesCreator;
    private Map<String, Set<String>> parentIdToUserOrGroupIds;
    private Queueable nextQueueable;
    private List<SObject> shareRecords;

    /**
    *   @param sharesCreator specifies the concrete type of Share records.
    *   @param parentIdToUserOrGroupIds Id of Sobject to be shared => Set<Id> of user or group  to be shared with.
    */
    public InsertSharesQueueable(SharesCreator sharesCreator, Map<String, Set<String>> parentIdToUserOrGroupIds) {
        this.sharesCreator = sharesCreator;
        this.parentIdToUserOrGroupIds = parentIdToUserOrGroupIds;
        this.shareRecords  = new List<SObject>(); 
    }

    /**
    *   @param sharesCreator specifies the concrete type of Share records.
    *   @param parentIdToUserOrGroupIds Id of Sobject to be shared => Set<Id> of user or group  to be shared with.
    *   @param nextQueueable another job enables you to chaining jobs.
    */
    public InsertSharesQueueable(SharesCreator sharesCreator, Map<String, Set<String>> parentIdToUserOrGroupIds, Queueable nextQueueable) {
         this(sharesCreator, parentIdToUserOrGroupIds);
         this.nextQueueable = nextQueueable;
    }

    /**
    *   Creates and inserts Share records in asynchronous Apex.
    *   @param context specifies the data for job execute.
    */ 
    public void execute(QueueableContext context) {
        shareRecords = sharesCreator.createShares(parentIdToUserOrGroupIds);
        insert shareRecords;
        if(null != nextQueueable){
            System.enqueueJob(nextQueueable);
        }
    }
}