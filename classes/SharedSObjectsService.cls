/**
 *  Encapsulates methods of Share objects and records actions.
 *  @author Maksym Gorinshteyn
 */
public class SharedSObjectsService {
    //Standard SObjects with specific fields of Share object
    private static final Set<String> specificSObjects = new Set<String>{
            SObjectType.Asset.Name, SObjectType.Campaign.Name,
            SObjectType.Case.Name, SObjectType.Contact.Name, SObjectType.Lead.Name,
            SObjectType.Opportunity.Name, SObjectType.User.Name, SObjectType.Account.Name
    };
    private static final String sharePostfix = 'share';
    private static final String underscoresSharePostfix = '__share';
    private static final String underscoresPostfix = '__c';
    // default value of ParentId field for Share object.
    private static String parentIdField = 'ParentId';

    /**
     *  Creates List of all Sobject's labels in org which have Share objects according to OWD settings.
     *  @return list of Share objects labels.
     */
    public static List<String> getSharedSObjectLabels() {
        List<String> sharedSObjectLabels = new List<String>();
        List<String> sObjectNames = new List<String>(Schema.getGlobalDescribe().keySet());
        for (String name : sObjectNames) {
            if (name.endsWith(sharePostfix)) {
                sharedSObjectLabels.add(name.remove(underscoresSharePostfix).remove(sharePostfix));
            }
        }
        return sharedSObjectLabels;
    }

    /**
     *  Creates list of wrappers contains all records of concrete SObject with existed shares.
     *  @param sObjType  specifies the concrete type of SObject records.
     *  @param userOgroupId specifies user or group Id SObject records are shared with.
     *  @param searchPattern specifies user's search input.
     *  @return List of Share object's labels.
     */
    public static List<RecordWrapper> getSharedSObjectRecords(Schema.SObjectType sObjType, String userOrGroupId, String searchPattern) {
        List<RecordWrapper> wrappedRecords = new List<RecordWrapper>();
        String sObjectName = String.valueOf(sObjType);
        String query = 'SELECT Id,Name FROM ' + sObjectName;
        if (!String.isBlank(searchPattern)) {
            query = 'SELECT Id,Name FROM ' + sObjectName + ' WHERE Name LIKE \'' + searchPattern + '%\'';
        }
        List <SObject> sharedSObjectsRecords = Database.query(query);
        List<SObject> shares = getSObjectShares(sObjType, userOrGroupId);
        List<Id> shareParentIds = new List<Id>();
        for (SObject share : shares) {
            shareParentIds.add((Id) share.get(parentIdField));
        }
        for (SObject record : sharedSObjectsRecords) {
            if (shareParentIds.contains(record.Id)) {
                wrappedRecords.add(new RecordWrapper(record, true));
            } else {
                wrappedRecords.add(new RecordWrapper(record, false));
            }
        }
        return wrappedRecords;
    }

    /**
     *  Creates list of Share records to be deleted.
     *  @param sObjType  specifies the concrete type of SObject records.
     *  @param deleteIds specifies Ids of Sobject records to be deleted from shares.
     *  @param roleId specifies concrete UserRole for query.
     *  @return list of Share records for delete.
     */
    public static List<SObject> getSharesToDelete(Schema.SObjectType sObjType, List<String> deleteIds, String roleId) {
        List<SObject> deleteShares = new List<SObject>();
        List<SObject> shares = getSObjectShares(sObjType, roleId);
        for (SObject share : shares) {
            if (deleteIds.contains((String) share.get(parentIdField))) {
                deleteShares.add(share);
            }
        }
        return deleteShares;
    }

    /**
    *  Creates list of Share records.
    *  @param sObjType  specifies the concrete type of SObject records.
    *  @param userOrGroupId specifies concrete user or group for query.
    *  @return list of Share records.
    */
    private static List<SObject> getSObjectShares(Schema.SObjectType sObjType, String userOrGroupId) {
        String sObjName = String.valueOf(sObjType);
        String sObjShareType;
        if (sObjName.endsWith(underscoresPostfix)) {
            sObjShareType = sObjName.remove(underscoresPostfix) + underscoresSharePostfix;
        } else {
            sObjShareType = sObjName + sharePostfix;
        }
        if (specificSObjects.contains(sObjName)) {
            parentIdField = sObjName + 'Id';
        }
        Group roleGroup = Database.query('SELECT Id From Group WHERE RelatedId =\'' + userOrGroupId + '\' AND Type=\'Role\'');
        List<SObject> shares = Database.query('SELECT Id, ' + parentIdField + ' FROM ' + sObjShareType + ' WHERE UserOrGroupId =\'' + roleGroup.Id + '\'');
        return shares;
    }

    //TODO description tells that there should be sObject record and its share sObject while the wrapper contains only one sObject and one boolean fields.
    //Creates wrapper of Sobject record and information whether it is shared.
    public class RecordWrapper {
        @AuraEnabled
        public SObject record;
        @AuraEnabled
        public Boolean shared;

        RecordWrapper(SObject record, Boolean shared) {
            this.record = record;
            this.shared = shared;
        }
    }
}