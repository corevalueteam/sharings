@isTest
private class InsertSharesQueueableTest {
    
    @testSetup 
    static void setup() {
        List<Account> accounts = new List<Account>();
        Integer sharesNumber = 100;
        for (Integer i = 0; i < sharesNumber; ++i) {
            accounts.add(new Account(Name='Test Account'+i));
        }
        insert accounts;
    }
    
    @isTest 
    static void testQueueable() {
        Map<String, Set<String>> parentIdToUserOrGroupIds = new Map<String, Set<String>>();
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test Account%'];
        Group testGroup = new Group();
        testGroup.Name = 'Test Group';
        insert testGroup;
        System.debug(testGroup.Id);
        Set<String> userOrGroupIds = new Set<String>{testGroup.Id};
        for(Account account:accounts){
            parentIdToUserOrGroupIds.put(account.Id, userOrGroupIds);    
        }
        Schema.SObjectType shareType= AccountShare.getSobjectType();
        if(shareType!=null){
            AccountSharesCreator accountSharesCreator = new AccountSharesCreator();
            String userOrGroupId = SObjectType.AccountShare.fields.UserOrGroupId.Name;
            InsertSharesQueueable asyncJob = new InsertSharesQueueable(accountSharesCreator, parentIdToUserOrGroupIds);
            InsertSharesQueueable anotherAsyncJob = new InsertSharesQueueable(accountSharesCreator, parentIdToUserOrGroupIds, null);
            Test.startTest();        
            System.enqueueJob(asyncJob);
            System.enqueueJob(anotherAsyncJob);
            Test.stopTest();
            Integer querySharesSize = Database.countQuery('SELECT count() FROM ' +shareType+ ' WHERE '+userOrGroupId+' = \''+testGroup.Id+'\'');
            System.assertEquals(accounts.size(),querySharesSize);
        }
    }
    

    
}