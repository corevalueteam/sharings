@isTest
private class SObjectSharesCreatorTest {
	
	@isTest 
	static void testCreateShare() {
		CustomObject__c obj = new CustomObject__c(Name = 'Test');
		insert obj;
		String objName = SObjectType.CustomObject__Share.Name;
		Schema.SObjectType shareType = Schema.getGlobalDescribe().get(objName);
		if(shareType!=null){
			SObjectSharesCreator sharesCreator = new SObjectSharesCreator(shareType);
			String userOrGroupIdFieldName = SObjectType.CustomObject__Share.fields.UserOrGroupId.Name;
			String  userOrGroupId = UserInfo.getUserId();
			Test.startTest();
			Sobject share = sharesCreator.createShare(obj.Id, userOrGroupId);
			Test.stopTest();
			System.assertEquals(true, share instanceof CustomObject__Share);
			System.assertEquals(userOrGroupId, share.get(userOrGroupIdFieldName));
		}
	}
	
}