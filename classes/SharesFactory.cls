/**
 *  Creates concrete implementation of SharesCreator accordingly to the SObject type.
 *
 *  @author Bogdan Protsenko
 *  @author Maksym Gorinshteyn
 */
public class SharesFactory {
	//Standard SObjects with specific fields of Share object (see StandardSObjectSharesCreator).
	private static final Set<String> specificSObjects = new Set<String>{
											SObjectType.Asset.Name, SObjectType.Campaign.Name, 
											SObjectType.Case.Name, SObjectType.Contact.Name, SObjectType.Lead.Name, 
											SObjectType.Opportunity.Name, SObjectType.User.Name
										};

	/**
	 *  Creates an implementation of SharesCreator for specified SObject type.
	 *  @param sObjType  specifies the concrete type of Share records.
	 *  @return sharesCreator implementation of SharesCreator interface.
	 */																						
	public static SharesCreator createSharesCreator(Schema.SObjectType sObjType){
		SharesCreator sharesCreator;
		Schema.SObjectType shareType;
        String sobjectName = sObjType.getDescribe().getName();
        if (sobjectName.endsWith('Account')){
        	sharesCreator = new AccountSharesCreator();
        } else if(specificSObjects.contains(sobjectName)){
        	shareType = Schema.getGlobalDescribe().get(sobjectName + 'Share');
        	sharesCreator = new StandardSObjectSharesCreator(shareType);
        } else {
			if(sobjectName.endsWith('__c')) {
				shareType = Schema.getGlobalDescribe().get(sobjectName.remove('__c') +'__Share');
			} else {
				shareType = Schema.getGlobalDescribe().get(sobjectName + 'Share');
			}
			sharesCreator = new SObjectSharesCreator(shareType);
		}
        return sharesCreator;
	}	
}