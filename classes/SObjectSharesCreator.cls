/**
 *  Creates Share records of all custom and standard sObject, excluding
 *  Asset, Campaign, Case, Contact, Lead, Opportunity and User.
 *  This creator does not follow any specific patterns for parentId nor accessLevel fields.
 *
 *  @author Bogdan Protsenko
 *  @author Maksym Gorinshteyn
 */
public class SObjectSharesCreator extends AbstractSharesCreator {

    /**
     *   @param shareType specifies the concrete type of Share records.
     */
    public SObjectSharesCreator (Schema.SObjectType shareType){
        super(shareType);
    }

    /**
     *  Creates single Share record between parent sObject and user or group.
     *  @param parentId identifies parent sObject to be shared.
     *  @param userOgroupId identifies user or group to be shared with.
     */
    public SObject createShare(String parentId, String userOrGroupId){
        SObject share = shareType.newSObject();
        share.put('ParentId', parentId);
        share.put('UserOrGroupId', userOrGroupId);
        share.put('AccessLevel', accessLevel);
        share.put('RowCause', rawCause);
        return share;
    }
}