/**
 *  Any Sobject Share records creation policy.
 *  @author Bogdan Protsenko
 *  @author Maksym Gorinshteyn
 */
public interface SharesCreator{

    /**
     *  Creates multiple Share records between parent sObject and user or group.
     *  @param parentIdToUserOrGroupId Id of Sobject to be shared => Set<Id> of user or group to be shared with.
     *  @return List<SObject> list of Share records.
     */
    List<SObject> createShares(Map<String, Set<String>> parentIdToUserOrGroupIds);

    /**
     *  Creates single Share record  between parent sObject and user or group.
     *  @param parentId record Id to be shared.
     *  @param userOgroupId user or group id  to be shared with.
     *  @return SObject Share record.
     */
    SObject createShare(String parentId, String userOgroupId);
}