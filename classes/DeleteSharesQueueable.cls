/**
 *  Queueable that deletes share records.
 *
 *  @author Bogdan Protsenko
 *  @author Maksym Gorinshteyn
 */
public with sharing class DeleteSharesQueueable implements Queueable {
    private Queueable nextQueueable;
    private List<SObject> shareRecords;

    /**
    *   @param shareRecords Share records for delete.
    */
    public DeleteSharesQueueable(List<SObject> shareRecords) {
        this.shareRecords  = shareRecords;
    }

    /**
    *   @param shareRecords Share records for delete.
    *   @param nextQueueable another job enables you to chaining jobs.
    */
    public DeleteSharesQueueable(List<SObject> shareRecords, Queueable nextQueueable) {
        this(shareRecords);
        this.nextQueueable = nextQueueable;
    }

    /**
    *   Creates and deletes Share records in asynchronous Apex.
    *   @param context specifies the data for job execute.
    */
    public void execute(QueueableContext context) {
        delete shareRecords;
        if(null != nextQueueable){
            System.enqueueJob(nextQueueable);
        }
    }
}