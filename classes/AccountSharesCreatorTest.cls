@isTest
private class AccountSharesCreatorTest {

    @isTest
    static void testCreateShare() {
        Account account = new Account(Name = 'Test');
        insert account;
        Schema.SObjectType shareType = AccountShare.getSobjectType();
        if (shareType != null) {
            AccountSharesCreator accountSharesCreator = new AccountSharesCreator();
            String userOrGroupIdFieldName = SObjectType.AccountShare.fields.UserOrGroupId.Name;
            String userOrGroupId = UserInfo.getUserId();
            String accountId = SObjectType.AccountShare.fields.AccountId.Name;
            Test.startTest();
            Sobject share = accountSharesCreator.createShare(account.Id, UserInfo.getUserId());
            Test.stopTest();
            System.assert(share instanceof AccountShare);
            System.assertEquals(userOrGroupId, share.get(userOrGroupIdFieldName));
            System.assertEquals(account.Id, share.get(accountId));
        }
    }
}