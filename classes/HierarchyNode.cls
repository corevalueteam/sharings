/**
 *  Represents a node of lightning:tree.
 *  @author Maksym Gorinshteyn
 */
public with sharing class HierarchyNode {

    //Node name(SObject record name).
    @AuraEnabled
    public String label;

    //Nested nodes.
    @AuraEnabled
    public List<Object> items;

    //Node Id(Sobject Id).
    @AuraEnabled
    public String name;

    //Specifies whether a branch is expanded in lightning.
    @AuraEnabled
    public Boolean expanded;

    public HierarchyNode(String label, String name, List<Object> items ) {
        this.label = label;
        this.name = name;
        this.items = items;
        this.expanded = false;
    }
}