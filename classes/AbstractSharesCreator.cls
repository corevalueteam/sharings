/**
 *  Encapsulates generic fields and methods of Share records creation.
 *
 *  @author Bogdan Protsenko
 *  @author Maksym Gorinshteyn
 */
public abstract class AbstractSharesCreator implements SharesCreator {
    //default values.
    public String accessLevel = 'Read';
    public String rawCause = 'Manual';

    protected Schema.SObjectType shareType;
    
    /**
    *  @param shareType specifies the concrete type of Share records.
    */
    public AbstractSharesCreator(Schema.SObjectType shareType) {
        if (shareType == null) {
            NoAccessException e = new NoAccessException();
            //InvalidParameterValueException e = new InvalidParameterValueException('No such Share in the org. Please, check OWD sharing settings','111');
           // e.setMessage('No such Share in the org. Please, check OWD sharing settings');
            throw e;
        }
        this.shareType = shareType;
    }

    /**
     *  Creates multiple Share records for parent SObject.
     *  @param parentIdToUserOrGroupId Id of Sobject to be shared => Set<Id> of user or group  to be shared with.
     *  @return List<SObject> list of Share records.
     */
    public List<SObject> createShares(Map<String, Set<String>> parentIdToUserOrGroupIds) {
        List<SObject> shares = new List<SObject>();
        List<String> parentIds = new List<String>(parentIdToUserOrGroupIds.keySet());
        List<String> userOrGroupIds;
        for (Integer i = 0, size = parentIds.size(); i < size; ++i) {
            userOrGroupIds = new List<String>(parentIdToUserOrGroupIds.get(parentIds[i]));
            for (Integer j = 0, length = userOrGroupIds.size(); j < length; ++j) {
                shares.add(createShare(parentIds[i], userOrGroupIds[j]));
            }
        }
        return shares;
    }
    
    /**
     *  Creates single Share record for assosiated SObject.
     *  @param parentId record Id to be shared.
     *  @param userOgroupId user or group Id for sharing.
     *  @return SObject Share record.
     */
    abstract SObject createShare(String parentId, String userOgroupId);
}