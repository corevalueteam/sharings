({

    /**
     * Initialization
     */
    doInit: function (component, event, helper) {
        helper.getHierarchyItems(component);
        helper.getSharesSObjectsLabels(component);
    },

    /**
     * UserRole click event.
     */
    onSelectNode: function (component, event, helper) {
        var selectedNodeId = event.getParam('name');
        component.set("v.records", null);
        component.set("v.selectedNodeId", selectedNodeId);
    },

    /**
     * SObject select event.
     */
    onSelectSObject: function (component, event, helper) {
        var sharedSobject = component.find("sharedSobject").get("v.value");
        component.set("v.selectedSObject", sharedSobject);
        var selectedNodeId = component.get("v.selectedNodeId");
        component.set("v.searchPattern", null);
        if (selectedNodeId === undefined) {
            //alert("Please, choose a UserRole SObject should be shared with");
            //this.showToast();
        } else {
            helper.getSObjectRecords(component, sharedSobject, selectedNodeId);
        }
    },

    search: function (component, event, helper) {
        var sharedSobject = component.find("sharedSobject").get("v.value");
        var selectedNodeId = component.get("v.selectedNodeId");
            helper.getSObjectRecords(component, sharedSobject, selectedNodeId);
    },

    /**
     * Select all event.
     */
    selectAll: function (component, event, helper) {
        helper.selectAllCheckboxes(component, event);

    },

    /**
     * Initiates the save process of selected shares.
     */
    shareSobjects: function (component, event, helper) {
        component.set("v.isOpen", false);
        helper.shareSObjects(component, event);

    },

    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.modalOpen", true);
    },

    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"
        component.set("v.modalOpen", false);
    },

    showSpinner: function (component, event) {
        component.set("v.spinner", true);
    },

    hideSpinner: function (component, event) {
        component.set("v.spinner", false);
    },

    showToast : function() {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": "Please, choose a UserRole SObject should be shared with."
        });
        toastEvent.fire();
    }
})