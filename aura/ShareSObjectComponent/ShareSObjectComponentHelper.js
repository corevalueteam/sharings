({
    getHierarchyItems: function (component) {
        var items = [];
        var sObjectName = component.get('v.sObjectName');
        var parentField = component.get('v.parentField');
        var action = component.get('c.getHierarchyNodes');
        action.setParams({
            sObjectName: sObjectName,
            parentField: parentField
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("state:" + state);
            if (state === "SUCCESS") {
                component.set("v.items", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    getSharesSObjectsLabels: function (component) {
        var action = component.get('c.getSharedSObjectNames');
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("state:" + state);
            if (state === "SUCCESS") {
                component.set("v.sharedSobjectsLabels", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    getSObjectRecords: function (component, sharedSobject, selectedNodeId) {
        var searchPattern = component.get("v.searchPattern");
            var action = component.get('c.getSharedSObjectRecords');
            action.setParams({
                sObjectName: sharedSobject,
                userOrGroupId: selectedNodeId,
                searchWord: searchPattern
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                console.log("state:" + state);
                if (state === "SUCCESS") {
                    component.set("v.records", response.getReturnValue());
                    if (response.getReturnValue().length == 0) {
                        component.set("v.message", true);
                       // component.set("v.result", false);
                    } else{
                        component.set("v.message", false);
                        component.set("v.result", true);
                    }
                    //component.set("v.searchKeyWord", null);
                }
                else {
                    console.log("Failed with state: " + response.getError());
                    component.set("v.result", false);
                }
            });
            $A.enqueueAction(action);
    },

    selectAllCheckboxes: function (component, event) {
        var selectedHeaderCheck = event.getSource().get("v.value");
        // return the List of all checkboxs element
        var getAllId = component.find("boxPack");
        // If the local ID is unique[in single record case], find() returns the component. not array
        if (!Array.isArray(getAllId)) {
            if (selectedHeaderCheck === true) {
                component.find("boxPack").set("v.value", true);
            } else {
                component.find("boxPack").set("v.value", false);
            }
        } else {
            // check if select all (header checkbox) is true then true all checkboxes on table in a for loop
            // if value is false then make all checkboxes false in else part with play for loop
            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("boxPack")[i].set("v.value", true);
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("boxPack")[i].set("v.value", false);
                }
            }
        }
    },

    shareSObjects: function (component, event) {
        var insertIds = [];
        var deleteIds = [];
        // get all checkboxes
        var getAllIds = component.find("boxPack");
        // If the local ID is unique[in single record case], find() returns the component. not array
        if (!Array.isArray(getAllIds)) {
            if (getAllIds.get("v.value") == true) {
                insertIds.push(getAllIds.get("v.text"));
            }
        } else {
            // if value is checked(true) then add those Id (store in Text attribute on checkbox) in insertIds var.
            // if value is checked(false) - in deleteIds var.
            for (var i = 0; i < getAllIds.length; i++) {
                if (getAllIds[i].get("v.value") == true) {
                    insertIds.push(getAllIds[i].get("v.text"));
                } else {
                    deleteIds.push(getAllIds[i].get("v.text"));
                }
            }
        }
        this.sharing(component, insertIds, deleteIds);
    },

    sharing: function (component, insertIds, deleteIds) {
        var sharedSobject = component.get("v.selectedSObject");
        var selectedNodeId = component.get("v.selectedNodeId");
        var action = component.get('c.shareSObjects');
        action.setParams({
            sObjectName: sharedSobject,
            insertIds: insertIds,
            deleteIds: deleteIds,
            roleId: selectedNodeId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("state:" + state);
            if (state === "SUCCESS") {
            }
            else {
                console.log("Failed with state: " + response.getError());
            }
            this.getSObjectRecords(component, sharedSobject, selectedNodeId );
        });
        $A.enqueueAction(action);
    }
})